#!/bin/bash

#==============================================================
# userdata.tpl
#==============================================================

# This is a template that is used to generate the userdata.sh
# that will be run on the application server when it is built.
# The variables are replaced during a terraform apply.

#--------------------------------------------------------------
# Environment Variables
#--------------------------------------------------------------

# Set the variables to the values passed in by bitbucket.
# Environment variables in the form TF_VAR_var_name are 
# inserted into this file by terraform.

export TEMP_VARIABLE=${efs_backup_bucket}

#--------------------------------------------------------------
# Install Packages
#--------------------------------------------------------------

# In a production system this would be baked into the AMI, 
# Look into our packer examples to see how this can be done.

# Install Apache (webserver) and nfs-common (used to mount the efs)
sudo apt-get -y update 
sudo apt-get install -y nfs-common apache2 awscli

#--------------------------------------------------------------
# Elastic File System
#--------------------------------------------------------------

# Remove existing default sites so we can mount the efs in its place
sudo rm -rf /var/www/html
sudo mkdir /var/www/html

# Mount the EFS
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${efs_id}.efs.ap-southeast-2.amazonaws.com:/ /var/www/html/

#--------------------------------------------------------------
# Configure Webpage
#--------------------------------------------------------------

# Create a simple web page to prove the efs has been mounted

echo "<h1>Hello World</h1>" | sudo tee -a /var/www/html/index.html


#--------------------------------------------------------------
# Backup script
#--------------------------------------------------------------
aws configure set s3.signature_version s3v4
cd /tmp

cat << 'EOF' >> efs-backup.sh
#!/bin/bash

#==============================================================
# backup-efs.sh
#==============================================================

# This script will create a tarball of the efs directory and
# upload it to an s3 bucket (specified during the terraform build)

export LAST_BACKUP=`date +\%F`

# Create Tarball
cd /var/www/html
sudo touch "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz
sudo tar -cvzf "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz . --exclude="$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz

# Upload to bucket (n.b. the bucket name is inserted during terraform apply)
aws s3api put-object --server-side-encryption AES256 --body "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz --key "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz --bucket "${efs_backup_bucket}"

sudo rm -f "$HOSTNAME"-efs-"$LAST_BACKUP".tar.gz
EOF

# Move backup script to an executable place
sudo mv -f /tmp/efs-backup.sh /usr/local/bin/efs-backup.sh
sudo chmod +x /usr/local/bin/efs-backup.sh

# create cron job to backup nightly
crontab -l >mycron
# 1 am AEST
echo "00 15 * * * efs-backup.sh" >> mycron
crontab mycron
rm mycron

#--------------------------------------------------------------
# Cleanup Environment Variables
#--------------------------------------------------------------

# remove variables
unset TEMP_VARIABLE