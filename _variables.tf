#==============================================================
# variables.tf
#==============================================================

# This file is used to set variables that are passed to sub 
# modules to build our stack

#--------------------------------------------------------------
# Terraform Remote State
#--------------------------------------------------------------

# Define the remote objects that terraform will use to store
# state. We use a remote store, so that you can run destroy
# from a seperate machine to the one it was built on.

terraform {
  required_version = ">= 0.12.1"

  backend "s3" {
    # This is an s3bucket you will need to create in your aws 
    # space
    bucket = "ga-devs-tfstate"

    # The key should be unique to each stack, because we want to
    # have multiple enviornments alongside each other we set
    # this dynamically in the bitbucket-pipelines.yml with the
    # --backend
    key = "three-tier-efs-dev"

    region = "ap-southeast-2"

    # This is a DynamoDB table with the Primary Key set to LockID
    dynamodb_table = "terraform-lock"

    #Enable server side encryption on your terraform state
    encrypt = true
  }
}

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Variables used in the global config

variable "region" {
  description = "The AWS region we want to build this stack in"
  default     = "ap-southeast-2"
}

variable "stack_name" {
  description = "The name of our application"
  default     = "two-tier-efs"
}

variable "owner" {
  description = "A group email address to be used in tags"
  default     = "autobots@ga.gov.au"
}

variable "environment" {
  description = "Used for seperating terraform backends and naming items"
  default     = "dev"
}

variable "availability_zones" {
  description = "Geographically distanced areas inside the region"

  default = {
    "0" = "ap-southeast-2a"
    "1" = "ap-southeast-2b"
    "2" = "ap-southeast-2c"
  }
}

variable "key_name" {
  description = "AWS EC2 Keypair to be configured on the servers"
  default     = "INSERT_YOUR_KEYPAIR_HERE"
}

variable "http_ip_address" {
  description = "Lock down http/s to this address when environment is dev / test"

  default = {
    "dev"  = "0.0.0.0/0"
    "test" = "0.0.0.0/0"
    "prod" = "0.0.0.0/0"
  }
}

#--------------------------------------------------------------
# Server Images
#--------------------------------------------------------------

# Search for our server images
# If this fails you will need to build it in your space

data "aws_ami" "ga_ubuntu" {
  most_recent = true
  name_regex  = "^Geoscience Australia Standard Apache Image.*"
  owners      = ["121695370940"]
}
