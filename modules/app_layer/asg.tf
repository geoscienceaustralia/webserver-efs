#==============================================================
# App / asg.tf
#==============================================================

# Autoscaling group to host our application server.

resource "aws_autoscaling_group" "asg" {
  lifecycle {
    create_before_destroy = true
  }

  vpc_zone_identifier   = aws_subnet.private.*.id
  name                  = "${var.stack_name}-${var.environment}-asg"
  min_size              = var.asg_min
  max_size              = var.asg_max
  force_delete          = true
  launch_configuration  = aws_launch_configuration.lc.name
  load_balancers        = [var.elb_name]
  health_check_type     = "ELB"

  tag {
    key                 = "Name"
    value               = "${var.stack_name}-${var.environment}-asg"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "owner"
    value               = var.owner
    propagate_at_launch = "true"
  }

  tag {
    key                 = "environment"
    value               = var.environment
    propagate_at_launch = "true"
  }

  tag {
    key                 = "stack_name"
    value               = var.stack_name
    propagate_at_launch = "true"
  }

  tag {
    key                 = "created_by"
    value               = "terraform"
    propagate_at_launch = "true"
  }
}
