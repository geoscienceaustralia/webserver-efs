#==============================================================
# Shared / vpc.tf
#==============================================================

# Create the Virtual Private Cloud that the rest of our 
# infratructure will be built in.

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name        = "${var.stack_name}-${var.environment}-vpc"
    owner       = var.owner
    stack_name  = var.stack_name
    environment = var.environment
    created_by  = "terraform"
  }
}

#--------------------------------------------------------------
# Internet Gateway
#--------------------------------------------------------------

# Create an internet gateway so the VPC can talk to the wider
# internet.

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "${var.stack_name}-${var.environment}-vpc"
    owner       = var.owner
    stack_name  = var.stack_name
    environment = var.environment
    created_by  = "terraform"
  }
}

#--------------------------------------------------------------
# Backup Bucket
#--------------------------------------------------------------

# Create a backup bucket with a lifecycle of 30 days.
# as the objects in the bucket aren't managed by terraform you 
# will need to manually delete all backups before destroying

resource "aws_s3_bucket" "efs_backup_bucket" {
  bucket = "${var.stack_name}-${var.environment}-efs-backup-bucket"
  acl    = "private"

  lifecycle_rule {
    id      = "backup30days"
    enabled = true
    prefix  = ""

    expiration {
      days = 30
    }
  }
}
