describe route_table(ENV['TF_VAR_stack_name'] + '-public-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2a') do
  it { should exist }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
  it { should have_route('10.0.0.0/16').target(gateway: 'local') }
  it { should have_subnet(ENV['TF_VAR_stack_name'] + '-public-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2a') }
end

describe route_table(ENV['TF_VAR_stack_name'] + '-public-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2b') do
  it { should exist }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
  it { should have_route('10.0.0.0/16').target(gateway: 'local') }
  it { should have_subnet(ENV['TF_VAR_stack_name'] + '-public-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2b') }
end

describe route_table(ENV['TF_VAR_stack_name'] + '-public-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2c') do
  it { should exist }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
  it { should have_route('10.0.0.0/16').target(gateway: 'local') }
  it { should have_subnet(ENV['TF_VAR_stack_name'] + '-public-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2c') }
end

describe route_table(ENV['TF_VAR_stack_name'] + '-private-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2b') do
  it { should exist }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
  it { should have_route('10.0.0.0/16').target(gateway: 'local') }
  it { should have_subnet(ENV['TF_VAR_stack_name'] + '-private-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2b') }
end

describe route_table(ENV['TF_VAR_stack_name'] + '-private-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2a') do
  it { should exist }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
  it { should have_route('10.0.0.0/16').target(gateway: 'local') }
  it { should have_subnet(ENV['TF_VAR_stack_name'] + '-private-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2a') }
end

describe route_table(ENV['TF_VAR_stack_name'] + '-private-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2c') do
  it { should exist }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
  it { should have_route('10.0.0.0/16').target(gateway: 'local') }
  it { should have_subnet(ENV['TF_VAR_stack_name'] + '-private-subnet-' + ENV['TF_VAR_environment'] + '-ap-southeast-2c') }
end