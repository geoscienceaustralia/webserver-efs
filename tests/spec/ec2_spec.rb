require 'awspec'
require 'ec2_helper'

describe ec2(EC2Finder.GetIdFromName(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-jumpbox')) do
  it { should exist }
  it { should be_running }
  its(:instance_type) { should eq 't2.nano' }
  it { should have_security_group('jump_ssh') }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
end

describe ec2(EC2Finder.GetIdFromName(ENV['TF_VAR_stack_name'] + '_' + ENV['TF_VAR_environment'] + '_asg')) do
  it { should exist }
  it { should be_running }
  its(:instance_type) { should eq 't2.micro' }
  it { should have_security_group('app_server') }
  it { should belong_to_vpc(ENV['TF_VAR_stack_name'] + '-' + ENV['TF_VAR_environment'] + '-vpc') }
end
