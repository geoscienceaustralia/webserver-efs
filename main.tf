#==============================================================
# main.tf
#==============================================================

# This file is used to configure global settings and create our
# stack by calling individual modules.
# This file is not executed in order, if a module relies on
# the outputs of another terraform will build it in that order.

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Configure the cloud provider and define the terraform backend

provider "aws" {
  version = "~> 2.43"
  region  = var.region
}

#--------------------------------------------------------------
# Shared Components
#--------------------------------------------------------------

# Create the Virtual Private Cloud and other global components.

module "shared" {
  source             = "./modules/shared"
  stack_name         = var.stack_name
  environment        = var.environment
  owner              = var.owner
  availability_zones = var.availability_zones
}

#--------------------------------------------------------------
# Public Layer
#--------------------------------------------------------------

# Create a public subnetwork with a jumpbox and elastic load
# balancer. Create security groups for the jumpbox and ELB. 

module "public" {
  source             = "./modules/public_layer"
  region             = var.region
  availability_zones = var.availability_zones
  stack_name         = var.stack_name
  environment        = var.environment
  owner              = var.owner

  # Network
  nat_gw_count    = "1"
  vpc_id          = module.shared.vpc_id
  igw_id          = module.shared.igw_id
  http_ip_address = var.http_ip_address

  # The ELB will connect to your server using this method
  instance_port     = "80"
  instance_protocol = "HTTP"
  healthcheck       = "HTTP:80/"
}

#--------------------------------------------------------------
# Application Layer
#--------------------------------------------------------------

# Create a private subnetwork with an autoscaling application
# server and a Elastic File System. Insert secrets into
# the userdata script so that the application mounts our EFS 
# and installs app.

module "app" {
  source             = "./modules/app_layer"
  availability_zones = var.availability_zones
  stack_name         = var.stack_name
  environment        = var.environment
  owner              = var.owner
  key_name           = var.key_name

  # Security Groups
  elb_sg_id      = module.public.elb_sg_id
  elb_name       = module.public.elb_name
  ngw_ids        = module.public.ngw_ids
  vpc_id         = module.shared.vpc_id
  asg_amis       = data.aws_ami.ga_ubuntu.id

  # Userdata script variables
  efs_backup_bucket = module.shared.efs_backup_bucket
}
